# 160620 - Google Transit - Combined JAN16-JUN16 Bus-ONLY Feed

#### 20 June 2016

This is the General Transit Feed of Metro’s (LACMTA’s) complete service for the period beginning June 26th, 2016 and ending December 10th, 2016.

## Evergreen link to this COMBINED (bus & rail) archive: [https://gitlab.com/LACMTA/gtfs_all/raw/master/gtfs_all.zip](https://gitlab.com/LACMTA/gtfs_all/raw/master/gtfs_all.zip)

As of May 6th, 2016 LA Metro is now publishing our Bus and Rail Services in separate Google Transit Exports ONLY. As a customer service, and to allow us to update these files more frequently, we have split these files up. The new rail-only export will be updated Daily (generally Tuesday – Saturday mornings), to allow us to send out more timely information and to allow our users to capture all temporary rail service changes that may occur on a daily basis.

Metro will provide bus-only exports with large-scale changes to the system: approximately once every one or two months. We will NOT continue to maintain the combined service feeds.

#### Evergreen link to the STANDALONE BUS archive: [https://gitlab.com/LACMTA/gtfs_bus/raw/master/gtfs_bus.zip](https://gitlab.com/LACMTA/gtfs_bus/raw/master/gtfs_bus.zip)

#### Evergreen link to the STANDALONE RAIL archive: [https://gitlab.com/LACMTA/gtfs_rail/raw/master/gtfs_rail.zip](https://gitlab.com/LACMTA/gtfs_rail/raw/master/gtfs_rail.zip)

## Contents
```
Archive:  gtfs_all.zip
  Length     Date   Time    Name
 --------    ----   ----    ----
      163  12-01-14 16:37   agency.txt
     8845  06-20-16 13:24   calendar.txt
    12331  06-20-16 14:00   calendar_dates.txt
    12441  06-20-16 13:29   routes.txt
 70847462  06-20-16 13:42   shapes.txt
302226627  06-20-16 13:38   stop_times.txt
   860392  06-20-16 13:51   stops.txt
  4974610  06-20-16 13:42   trips.txt
 --------                   -------
378942871                   8 files
```

----

## Subscribe

### Subscribe to the RSS feed for update notifications:
[https://gitlab.com/LACMTA/gtfs_all.atom](https://gitlab.com/LACMTA/gtfs_all.atom)

### See the latest commits date times:
[https://gitlab.com/LACMTA/gtfs_all/commits/master.atom](https://gitlab.com/LACMTA/gtfs_all/commits/master.atom)

### Get the latest commit ID with Curl

```
#!/bin/sh

url="https://gitlab.com/LACMTA/gtfs_all/commits/master.atom"

curl --silent "$url" | grep -E '(title>|updated>)' | \
  sed -n '4,$p' | \
  sed -e 's/<title>//' -e 's/<\/title>//' -e 's/<updated>/   /' \
      -e 's/<\/updated>//' | \
  head -2 | fmt

# returns:
# 2015-12-31T13:09:36Z
#    new info from SPA and instructions on preparing the archive
```

### Get the latest commit ID with Python

```
#!/bin/env python

import feedparser

url = 'https://gitlab.com/LACMTA/gtfs_all/commits/master.atom'
d = feedparser.parse(url)
lastupdate = d['feed']['updated']

print lastupdate
```
----

This is the official source for LA County Metro's General Transit Feed Specification (GTFS) data. Join our developer community at http://developer.metro.net to learn more about using this data.

See the [http://developer.metro.net/the-basics/policies/terms-and-conditions/](http://developer.metro.net/the-basics/policies/terms-and-conditions/) page for Metro's terms and conditions.



### Archive information

We produce this zip with the following command:

```
# make the google_transit.zip file
cd gtfs_all ; zip -r ../gtfs_all.zip . -x ".*" -x "*/.*"

# list contents
unzip -l gtfs_all.zip

Archive:  gtfs_all.zip
  Length     Date   Time    Name
 --------    ----   ----    ----
      163  12-01-14 16:37   agency.txt
     8845  06-20-16 13:24   calendar.txt
    12331  06-20-16 14:00   calendar_dates.txt
    12441  06-20-16 13:29   routes.txt
 70847462  06-20-16 13:42   shapes.txt
302226627  06-20-16 13:38   stop_times.txt
   860392  06-20-16 13:51   stops.txt
  4974610  06-20-16 13:42   trips.txt
 --------                   -------
378942871                   8 files
```
